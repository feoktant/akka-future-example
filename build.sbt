name := "akka-future-example"

version := "0.1"

scalaVersion := "2.13.7"

idePackagePrefix := Some("io.feoktant")

val AkkaVersion = "2.6.17"
val AkkaHttpVersion = "10.2.2"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor-typed" % AkkaVersion,
  "com.typesafe.akka" %% "akka-stream"      % AkkaVersion,
  "com.typesafe.akka" %% "akka-http"        % AkkaHttpVersion,
)