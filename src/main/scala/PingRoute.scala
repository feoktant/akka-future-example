package io.feoktant

import PingRoute.myExceptionHandler
import PingService.PingException

import akka.actor.ActorRef
import akka.http.scaladsl.model.HttpResponse
import akka.http.scaladsl.model.StatusCodes.{Accepted, InternalServerError, NotImplemented, ServiceUnavailable}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.{ExceptionHandler, Route => AkkaRoute}
import akka.pattern.ask
import akka.util.Timeout

import scala.concurrent.ExecutionContext
import scala.concurrent.duration.DurationInt
import scala.util.{Failure, Success}

class PingRoute(
  pingActor: ActorRef,
  pingService: PingService,
)(implicit ec: ExecutionContext) {

  val route: AkkaRoute = get {
    path("example-1") {
      // Пример 1. Нас устраивает возвращаемый тип
      //  - статус-код по умолчанию 200 ОК
      //  - возвращаемый тип легко серилизируется
      complete(pingService.ping())
    } ~
    path("example-2") {
      // Пример 2. Моделируем возврат ошибки
      //  - статус-код по умолчанию 500 ОК
      //  - ошибка по умолчанию
      complete(pingService.pingError())
    } ~
    path("example-3") {
      // Пример 3. Решаем по-разному реагировать на разные ошибки
      onComplete(pingService.pingError()) {
        case Success(str)             => complete(Accepted -> str)
        case Failure(PingException()) => complete(ServiceUnavailable -> "bla-bla")
        case Failure(_)               => complete(NotImplemented)
      }
    } ~
    path("example-4") {
      // Пример 4. Решаем вынести логику обработки ошибок в отдельную функцию,
      // что бы в последствии переиспользовать.
      // Его можно объявить implicit, и вообще не использовать handleExceptions
      handleExceptions(myExceptionHandler) {
        complete(pingService.pingError())
      }
    } ~
    path("example-akka") {
      // Пример 5. Спрашиваем у актора, и кастим результат что он вернул
      // mapTo возвращает Future, дальше с ней как обычно, как и с примерами выше.
      // Timeout иметь обязательно!
      implicit val to: Timeout = Timeout(5.seconds)
      complete {
        (pingActor ? "ping").mapTo[String]
      }
    }
  }

}

object PingRoute {

  // https://doc.akka.io/docs/akka-http/current/routing-dsl/exception-handling.html
  def myExceptionHandler: ExceptionHandler =
    ExceptionHandler {
      case PingException() => complete(ServiceUnavailable -> "Exception handler!")
      case _ => complete(HttpResponse(InternalServerError, entity = "Horrible disaster..."))
    }

}
