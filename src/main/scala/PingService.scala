package io.feoktant

import io.feoktant.PingService.PingException

import scala.concurrent.Future

class PingService {
  def ping(): Future[String] = Future.successful("Pong")
  def pingError(): Future[String] = Future.failed(PingException())
}

object PingService {
  case class PingException() extends Exception
}
