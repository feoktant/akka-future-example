package io.feoktant

import akka.actor.Actor

class PingActor extends Actor {

  override def receive: Receive = {
    case "ping" => sender() ! "pong!"
  }

}
