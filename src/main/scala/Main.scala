package io.feoktant

import akka.actor.{ActorSystem, Props}
import akka.http.scaladsl.Http

import scala.io.StdIn

object Main extends App {

  implicit val system = ActorSystem("my-system")
  implicit val executionContext = system.dispatcher

  val pingRoute = new PingRoute(system.actorOf(Props[PingActor]()), new PingService())

  val bindingFuture = Http().newServerAt("localhost", 8080).bind(pingRoute.route)

  println(s"Server now online. Please navigate to http://localhost:8080/hello\nPress RETURN to stop...")
  StdIn.readLine() // let it run until user presses return
  bindingFuture
    .flatMap(_.unbind()) // trigger unbinding from the port
    .onComplete(_ => system.terminate()) // and shutdown when done


}
